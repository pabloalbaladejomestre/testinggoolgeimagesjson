import CSV.CSVReader;
import CSV.CSVWriter;
import GoogleImage.GoogleImageClass;
import GoogleImage.GoogleImageService;

import JSON.JSONService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;

/**
 * Created by pablo on 13/10/14.
 */
public class main {
    public main() {
    }

    public static void main(String[] args) {
        //Files
        String inputFile = "//Users/pablo/Desktop/input.csv";
        String outputFile = "//Users/pablo/Desktop/output.csv";

        //IP
        String userip = "88.26.210.155";

        //Delay
        int maxDelayMs = 3000;
        int minDelayMS = 1000;
        //Query Parameters -- https://developers.google.com/image-search/v1/jsondevguide#json_args

        String version = "1.0";

        //RIGHTS
        //String rights = "cc_sharealike";
        String rights = "";

        //IMAGE SIZE
        //String imgsz = "small|medium|large|xlarge";//medium
        String imgsz = "xxlarge|huge";//large

        //IMAGE TYPE
        String imgtype = "photo";


        //FILE TYPE
        String as_filetype="jpg";

        //LANGUAGE
        String hl="en";

        //MAX RESULTS
        String rsz = "1";


        CSVReader csvReader = new CSVReader();
        csvReader.open(inputFile);

        CSVWriter csvWriter = new CSVWriter();
        csvWriter.open(outputFile);

        boolean stop = false;
        int currentRequest = 0;
        int numErrors = 0;

        while(!stop){

            String [] inputValues = csvReader.readNext();
            if(inputValues != null){
                currentRequest++;
                System.out.println("--- " + currentRequest + "("+numErrors+" errors)" + " ---");
                System.out.println("Place: "+inputValues[0]);

                String query = "\"" + inputValues[0] + "\"";
                query += " hospital";

                try {
                    query = URLEncoder.encode(query,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //URL BUILT
                String url = "https://ajax.googleapis.com/ajax/services/search/images?"
                        + "v="+version
                        + "&q=" + query
                        + "&userip=" + userip
                        + "&as_rights="+rights
                        + "&rsz="+rsz
                        + "&imgtype="+imgtype
                        + "&as_filetype="+as_filetype
                        + "&imgsz="+imgsz
                        + "&hl="+hl
                        ;

                System.out.println("Calling url: " + url);

                //Request
                JsonObject response = JSONService.getJsonObject(url);

                //Processing
                while(response.get("responseData").isJsonNull()){
                    try {
                        int delay = randInt(minDelayMS,maxDelayMs);
                        System.out.println("Error at " + currentRequest + " waiting " + delay + " ms");
                        Thread.sleep(delay);

                        response = JSONService.getJsonObject(url);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

                JsonObject responseData = response.get("responseData").getAsJsonObject();
                JsonArray results = responseData.get("results").getAsJsonArray();

                String [] outputValues = new String [6];
                outputValues [0] =  inputValues [0]; //Name

                if(results.size() == 0){
                    outputValues [1] =  null;
                    outputValues [2] =  null;
                    outputValues [3] =  null;
                    numErrors++;
                }else{
                    //The first one
                    GoogleImageClass image = GoogleImageService.getGoogleImage(results.get(0).getAsJsonObject());
                    outputValues [1] =  inputValues[4]; //city
                    outputValues [1] =  inputValues[7]; //country
                    outputValues [3] =  image.getUrl();
                    outputValues [4] =  Integer.toString(image.getWidth());
                    outputValues [5] =  Integer.toString(image.getHeight());
                    System.out.println("Image url:" + outputValues [3]);
                }
                csvWriter.append(outputValues);

                try {
                    int delay = randInt(minDelayMS,maxDelayMs);
                    System.out.println("Waiting " + delay + " ms for next request");
                    System.out.println("---------");
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }else{
                stop = true;
            }

        }

        csvReader.close();
        csvWriter.close();
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}

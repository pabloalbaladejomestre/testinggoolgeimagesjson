package CSV;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by pablo on 13/10/14.
 */

public class CSVReader {
    BufferedReader br = null;
    String cvsSplitBy = ",";

    public void open(String filePath){

        try {
            br = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void close(){

        if (br != null) try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String [] readNext(){
        if(br != null){
            try {
                String line = br.readLine();
                if(line != null){
                    return line.split(cvsSplitBy);
                }else{
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}

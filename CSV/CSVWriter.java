package CSV;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by pablo on 13/10/14.
 */
public class CSVWriter {

    BufferedWriter bw = null;
    String cvsSplitBy = ",";

    public void open(String filePath){
        try {
            bw = new BufferedWriter(new FileWriter(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        if(bw != null) {
            try {
                bw.flush();
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void append(String [] values){
        if(values != null && bw != null){
            String line = "";
            for(int i = 0; i < values.length; i++){
                line += values[i];
                if(i  < values.length -1) line +=cvsSplitBy;
            }
            line += '\n';
            try {
                bw.append(line);
                bw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

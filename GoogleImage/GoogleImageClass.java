package GoogleImage;

import com.google.gson.JsonObject;

/**
 * Created by pablo on 13/10/14.
 */
public class GoogleImageClass {

    private String url;
    private int width;
    private int height;

    public GoogleImageClass() {
        this.height = 0;
        this.width = 0;
        this.url = "";
    }

    public GoogleImageClass(int height, int width, String url) {
        this.height = height;
        this.width = width;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "GoogleImageClass{" +
                "url='" + url + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}


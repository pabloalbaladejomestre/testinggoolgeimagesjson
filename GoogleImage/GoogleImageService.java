package GoogleImage;

import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by pablo on 13/10/14.
 */
public class GoogleImageService {

    public static GoogleImageClass getGoogleImage(JsonObject jsonObject) {
        GoogleImageClass googleImageClass = new GoogleImageClass();

        try {
            googleImageClass.setUrl(URLDecoder.decode(jsonObject.get("url").getAsString(),"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        googleImageClass.setWidth(jsonObject.get("width").getAsInt());
        googleImageClass.setHeight(jsonObject.get("height").getAsInt());

        return googleImageClass;
    }
}
